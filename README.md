## Clone Repository and Build Image

Clone git repository https://gitlab.com/docker.public/rundeck.git

```
git clone https://gitlab.com/docker.public/rundeck.git
```

## Export variables

```
export RUNDECK_GRAILS_URL=http://domain.example.com
export LATEST_RUNDECK_VERSION=3.0.12
```

## Run Pentaho Server
```
cd rundeck/
docker-compose up -d
```